﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SocketsConSSL
{
    public class MemoriaRam
    {
        public double TotalFreeSpace { get; set; }
        public double TotalPhysicalMemory { get; set; }
    }
}
