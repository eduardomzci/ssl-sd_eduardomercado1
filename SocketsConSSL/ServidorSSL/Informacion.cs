﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServidorSSL
{
    public class Informacion
    {
        public List<Almacenamiento> Almacenamientos { get; set; }
        public MemoriaRam MemoriaRam { get; set; }
        public List<GPU> GPUs { get; set; }
        
    }
}
