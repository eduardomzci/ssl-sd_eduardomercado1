﻿using Microsoft.VisualBasic.Devices;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Management;
using System.Net;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ServidorSSL
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static int _hostPort = 2620;
        IPAddress ip = IPAddress.Parse("25.72.12.219");

        public MainWindow()
        {
            InitializeComponent();
            host.Content = ip.ToString();
            port.Content = _hostPort.ToString();
        }

        private void btnIniciarServer_Click(object sender, RoutedEventArgs e)
        {
            var serverCertificate = getServerCert();
            var listener = new TcpListener(ip, _hostPort);
            listener.Start();
            this.Dispatcher.Invoke(() =>
            {
                txtEstadoServer.Text += "EL Servidorse ha iniciado con exito. \n";
            });
            while (true)
            {
                using (var client = listener.AcceptTcpClient())
                using (var sslStream = new SslStream(client.GetStream(),
                   false, ValidateCertificate))
                {
                    sslStream.AuthenticateAsServer(serverCertificate,
                       true, SslProtocols.Tls12, false);
                    var inputBuffer = new byte[4096];
                    var inputBytes = 0;
                    while (inputBytes == 0)
                    {
                        inputBytes = sslStream.Read(inputBuffer, 0,
                           inputBuffer.Length);
                    }
                    this.Dispatcher.Invoke(() =>
                    {
                        var inputMessage = Encoding.UTF8.GetString(inputBuffer,
                       0, inputBytes);
                        MessageBox.Show("Se conecto: "+inputMessage);
                        if (inputMessage == "SocketCliente")
                        {
                            //infoSystem
                            Informacion info = new Informacion()
                            {
                                GPUs = new List<GPU>(),
                                Almacenamientos = new List<Almacenamiento>()
                            };
                            ManagementObjectSearcher myVideoObject = new ManagementObjectSearcher("select * from Win32_VideoController");
                            foreach (ManagementObject obj in myVideoObject.Get())
                            {
                                info.GPUs.Add(new GPU()
                                {
                                    Name = obj["Name"].ToString(),
                                    Status = obj["Status"].ToString(),
                                    AdapterRAM = obj["AdapterRAM"].ToString(),
                                    AdapterDACType = obj["AdapterDACType"].ToString(),
                                    DriverVersion = obj["DriverVersion"].ToString()
                                });
                            }
                            DriveInfo[] allDrives = DriveInfo.GetDrives();
                            foreach (DriveInfo d in allDrives)
                            {
                                if (d.IsReady == true)
                                {
                                    info.Almacenamientos.Add(new Almacenamiento()
                                    {
                                        TotalAvailableSpace = d.TotalFreeSpace,
                                        TotalSizeOfDrive = d.TotalSize,
                                        RootDirectory = d.RootDirectory.Name
                                    });
                                }
                            }
                            PerformanceCounter ram = new PerformanceCounter();
                            ComputerInfo infoDevice = new ComputerInfo();
                            ram.CategoryName = "Memory";
                            ram.CounterName = "Available Bytes";
                            info.MemoriaRam = new MemoriaRam()
                            {
                                TotalPhysicalMemory = infoDevice.TotalPhysicalMemory,
                                TotalFreeSpace = ram.NextValue()
                            };
                            string result = JsonConvert.SerializeObject(info);
                            byte[] data = Encoding.ASCII.GetBytes(result);
                            //current.Send(data);
                            Console.WriteLine("Info sent to client");
                            //fin codigo lenny
                            //var outputMessage = data;
                            //var outputBuffer = Encoding.UTF8.GetBytes(outputMessage); 
                            var outputBuffer = data;
                            sslStream.Write(outputBuffer);
                            //fin mandar especificaciones
                        }
                        txtEstadoServer.Text += string.Format("Recibido: {0} \n", inputMessage);
                        Console.WriteLine("Conectado con: {0}", inputMessage);
                    });
                }
            }
        }
        static bool ValidateCertificate(Object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            //return true;
            if (sslPolicyErrors == SslPolicyErrors.None)
            { return true; }
            if (sslPolicyErrors ==
                  SslPolicyErrors.RemoteCertificateChainErrors)
            { return true; }
            return false;
        }

        private static X509Certificate getServerCert()
        {
            X509Store store = new X509Store(StoreName.My,
               StoreLocation.CurrentUser);
            store.Open(OpenFlags.ReadOnly);

            X509Certificate2 foundCertificate = null;
            foreach (X509Certificate2 currentCertificate
               in store.Certificates)
            {
                if (currentCertificate.IssuerName.Name
                   != null && currentCertificate.IssuerName.
                   Name.Equals("CN=SocketSSLCertificate"))
                {
                    foundCertificate = currentCertificate;
                    break;
                }
            }


            return foundCertificate;
        }
    }
}
